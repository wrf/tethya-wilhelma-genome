# Overview #
*Tethya wilhelma* is an emerging laboratory model demosponge. The original description from 2001 can be found [here](http://www-alt.naturkundemuseum-bw.de/stuttgart/pdf/a_pdf/A631.pdf). Technical details and raw data for the *Tethya wilhelma* sequencing project can be found at the NCBI BioProject [PRJNA288690](http://www.ncbi.nlm.nih.gov/bioproject/PRJNA288690).

Genomic data and all annotations have been moved to the [MolPalMuc Tethya genome page](https://bitbucket.org/molpalmuc/tethya_wilhelma-genome).

# Gene trees #
Sequence accessions are almost entirely unchanged from the downloaded versions (Ensembl, NCBI, Uniprot). Where species name is not part of the fasta header:

* Bf_V2_XXX, bflornaseq, or [BRAFL](http://genome.jgi.doe.gov/Brafl1/Brafl1.home.html) = Branchiostoma floridae
* [PFL3](https://groups.oist.jp/molgenu/hemichordate-genomes) = Ptychodera flava
* DROME, FB12345 (FlyBase) = Drosophila melanogaster
* [Dappu1](http://genome.jgi.doe.gov/Dappu1/Dappu1.home.html) = Daphnia pulex
* BGIBMGA012345 = Bombyx mori
* NV12345 = Nasonia vitripennis (note similarity to NVE12345)
* Amel_4.5, Amel_v1, APIME = Apis mellifera
* TC012345 = Tribolium castaneum
* [Ocbimv22012345m.p](https://groups.oist.jp/molgenu/octopus-genome), obirnaseq = Octopus bimaculoides
* [Helro1](http://genome.jgi.doe.gov/Helro1/Helro1.home.html) = Helobdella robusta
* [Lotgi1](http://genome.jgi.doe.gov/Lotgi1/Lotgi1.home.html) = Lottia gigantea
* [Capca1](http://genome.jgi.doe.gov/Capca1/Capca1.home.html) = Capitella teleta
* Cgigas, EKC12345 = Crassostrea gigas
* [Lingula_comp1234](http://marinegenomics.oist.jp/lingula/download/lingula_transcriptome_v1.0_prot.fa.gz) = Lingula anatina

* [ML12345a](http://research.nhgri.nih.gov/mnemiopsis/download/proteome/ML2.2.aa.gz) = Mnemiopsis leidyi
* [Triad1](http://genome.jgi.doe.gov/Triad1/Triad1.home.html), Tadherens = Trichoplax adherens
* Hhu, Hoilungia, or H13 = Hoilungia hongkongensis
* NVE12345 = Nematostella vectensis
* [AIPGENE12345](http://aiptasia.reefgenomics.org/download/) = Exaiptasia pallida (was Aiptasia sp)
* [adi_v1](http://marinegenomics.oist.jp/coral/download/adi_aug101220_pasa_prot.fa.gz) = Acropora digitifera
* [hydra_sra](https://bitbucket.org/wrf/genome-reannotations/downloads) = Hydra magnipapillata
* [scict1](http://compagen.org/datasets/SCIL_T-PEP_130802.zip) = Sycon ciliatum
* [lctid12345](http://compagen.org/datasets/LCOM_T-PEP_130802.zip) = Leucosolenia complicata
* [Aqu2.1](http://amphimedon.qcloud.qcif.edu.au/downloads.html) = Amphimedon queenslandica
* Twi = Tethya wilhelma

* [Monbr1](http://genome.jgi.doe.gov/Monbr1/Monbr1.home.html) = Monosiga brevicollis
* Srosetta, PTSG = Salpingoeca rosetta
* CAOG = Capsaspora owczarzaki
* SARC = Sphaeroforma arctica

# Other sponge transcriptomes #
Sponge re-assemblies used in the gene trees can be found [here](https://bitbucket.org/wrf/porifera-transcriptomes/downloads).

# Other cnidarian transcriptomes #
Cnidarian re-assemblies used in the gene trees can be found [here](https://bitbucket.org/wrf/cnidarian-transcriptomes/downloads).

# Experiments involving Tethya wilhelma #
*Tethya wilhelma* has been used as a biological model in many studies, including:

* Nickel, M. (2004) [Kinetics and rhythm of body contractions in the sponge Tethya wilhelma.](http://jeb.biologists.org/content/207/26/4515.short) The Journal of experimental biology, 207:4515-4524.
* Ellwanger, K. and Nickel, M. (2006) [Neuroactive substances specifically modulate rhythmic body
  contractions in the nerveless metazoon Tethya wilhelma.](https://frontiersinzoology.biomedcentral.com/articles/10.1186/1742-9994-3-7) Frontiers in zoology, 3:7.
* Nickel, M. (2006) [Like a 'rolling stone': quantitative analysis of the body movement and skeletal dynamics of the sponge Tethya wilhelma.](http://jeb.biologists.org/content/209/15/2839.long) The Journal of experimental biology, 209:2839-46.
* Ellwanger, K., Eich, A., and Nickel, M. (2007) [GABA and glutamate specifically induce contractions in the sponge
 Tethya wilhelma.](http://link.springer.com/article/10.1007/s00359-006-0165-y). Journal of Comparative Physiology A, 193(1): 1-11.
* Nickel, M. et al. (2011) [The contractile sponge epithelium sensu lato body contraction of the demosponge Tethya wilhelma is mediated by the pinacoderm](http://jeb.biologists.org/content/214/10/1692.short) Journal of Experimental Biology 214: 1692-1698.

For more information, also see the [website by PD Dr. Michael Nickel](http://www.porifera.net/), one of the scientists originally describing *Tethya wilhelma*.

# Resources on other Tethya species #
*Tethya* species are found worldwide. For a discussion of the phylogeny of this genus, see work by [Heim, Nickel and Brummer, 2007](https://doi.org/10.1017/S0025315407058419)

### Genomics ###
* The [mitochondrial genome is available](https://www.ncbi.nlm.nih.gov/nuccore/NC_006991) for *Tethya actinia*, see [Lavrov et al., 2005](https://academic.oup.com/mbe/article-lookup/doi/10.1093/molbev/msi108)

### Microbiome ###
Various *Tethya* species have been studied for microbial interactions:

* Thiel, V. et al (2007) [Spatial distribution of sponge-associated bacteria in the Mediterranean sponge Tethya aurantium.](https://academic.oup.com/femsec/article-lookup/doi/10.1111/j.1574-6941.2006.00217.x) FEMS Microbiology Ecology 59(1):47-63.
* Sipkema, D. and Blanch, HW. (2010) [Spatial distribution of bacteria associated with the marine sponge Tethya californiana.](https://www.ncbi.nlm.nih.gov/pubmed/24391242) Marine Biology 157(3):627-638.
* Simister, R. et al. (2013) [Temporal molecular and isotopic analysis of active bacterial communities in two New Zealand sponges.](https://academic.oup.com/femsec/article-lookup/doi/10.1111/1574-6941.12109) FEMS Microbiology Ecology 85(1):195-205.
* Cardenas, CA. et al. (2014) [Influence of environmental variation on symbiotic bacterial communities of two temperate sponges.](https://academic.oup.com/femsec/article-lookup/doi/10.1111/1574-6941.12317) FEMS Microbiology Ecology 88(3):516-27.

### Biomineralization ###
* Shimizu, K., et al. (1998) [Silicatein alpha: cathepsin L-like protein in sponge biosilica.](http://www.pnas.org/content/95/11/6234.long) Proc Natl Acad Sci 95(11):6234-8.

# Other sponge resources #
* Genome and gene sets of the first sequenced demosponge [Amphimedon queenslandica](http://amphimedon.qcloud.qcif.edu.au/downloads.html)
* Genome and gene sets of the calcisponge [Sycon ciliatum](http://compagen.org/datasets/SCIL_T-PEP_130802.zip)
* Transcriptome of the calcisponge [Leucosolenia complicata](http://compagen.org/datasets/LCOM_T-PEP_130802.zip)
* Transcriptome of the homoscleromorph [Oscarella carmela](http://compagen.org/datasets/OCAR_T-CDS_130911.zip)
* Genomic assemblies of *Xestospongia testutinaria* and *Stylissa carteri*, and many other species can be found at [reefgenomics.org](http://reefgenomics.org/)